# Polymer App Toolbox - Starter Kit

Tech University
Autor: Lídice Stankervicaite
2018, Uruguay
Versión 1.0.0

Descripción del Proyecto:  
En el proyecto se implementó una parte de una una aplicación bancaria para la cual se dispone de las siguientes opciones:

Login: Aunque internamente se maneja un nro. de cliente para cada usuario, de modo de facilitar la operativa el usuario estará identificado por su mail el cual es único en la base. El registro habilita a los usuarios registrados el resto de la operativa.

Logout: Para la desconexión del sistema.

Registrarse:Permite que el alta al sistema de nuevos usuarios, controlando que no exista en la base otro usuario con el mismo mail.

Baja usuario: Permite dar de baja un usuario, controlando que el mismo no tenga cuentas vinculadas.
Modificación usuario: Se muestran los datos actuales del usuario y en caso de que se desee se pueden modificar los mismos.

Consulta cuentas: Muestra todas las cuentas del usuario que está logueado.

Consulta movimientos: Se solicita la cuenta y se muestran los movimientos realizados con la misma.

Mapa: Link que lleva al mapa de google maps de Montevideo.

Usuarios registrados: Esta funcionalidad está pensada para un usuario administrador de la aplicación dado que lista de forma ordenada por id a todos los usuarios registrados al sistema.

Cotizaciones: Link que lleva a la página de cotizaciones de monedas del Banco Central.
